﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace UART_Testgelände
{
    public partial class Form1 : Form
    {
        private BitArray bitArray = new BitArray(10 * 35);
        private byte[] buff = new byte[35];
        private Data_new_Protokoll RX_Data = null;
        private Data_new_Protokoll test = null;
        bool datarecieved_flag = false;
        bool Challenge_flag = false;
        private int remaining_init_commands = 0;

        #region Konstanten
        private static Data_new_Protokoll Enter_Production_Mode_Request = new Data_new_Protokoll(0x02, new byte[] { 0xBA, 0xDC, 0x0D, 0xED });
        //private static Data_new_Protokoll Enter_Production_Mode_Response_ = new Data_new_Protokoll(0x03, new byte[] { });

        private static Data_new_Protokoll Exit_Production_Mode_Request = new Data_new_Protokoll(0x04, new byte[] { 0xDE, 0xAD, 0xC0, 0xDE });

        private static Data_new_Protokoll Callenge_Request = new Data_new_Protokoll(0x06, null);

        private static Data_new_Protokoll Query_Error_Status = new Data_new_Protokoll(0x0E, null);

        #endregion

        public Form1()
        {

            InitializeComponent();
            //Testwort einfügen!!!word.rawbitarray = ;
            #region
            bitArray[0] = true;
            bitArray[1] = false;
            bitArray[2] = false;
            bitArray[3] = false;
            bitArray[4] = false;
            bitArray[5] = false;
            bitArray[6] = false;
            bitArray[7] = true;
            bitArray[8] = false;

            bitArray[9] = false;
            bitArray[10] = true;
            bitArray[11] = false;
            bitArray[12] = false;
            bitArray[13] = false;
            bitArray[14] = false;
            bitArray[15] = false;
            bitArray[16] = false;
            bitArray[17] = false;

            bitArray[18] = true;
            bitArray[19] = false;
            bitArray[20] = false;
            bitArray[21] = false;
            bitArray[22] = false;
            bitArray[23] = false;
            bitArray[24] = false;
            bitArray[25] = true;
            bitArray[26] = false;

            bitArray[27] = true;
            bitArray[28] = false;
            bitArray[29] = false;
            bitArray[30] = false;
            bitArray[31] = false;
            bitArray[32] = false;
            bitArray[33] = false;
            bitArray[34] = true;
            bitArray[35] = false;

            bitArray[36] = true;
            bitArray[37] = false;
            bitArray[38] = false;
            bitArray[39] = false;
            bitArray[40] = false;
            bitArray[41] = false;
            bitArray[42] = false;
            bitArray[43] = true;
            bitArray[44] = false;
            #endregion
            //Bitfolge
            //buff[0] = 0x01;
            //buff[1] = 0x04;
            //buff[2] = 0x00;

            Sio.ErrorReceived += new SerialErrorReceivedEventHandler(sp_SerialErrorReceivedEventHandler);
            Sio.StopBits = StopBits.One;
            Sio.DataBits = 8;
            Sio.BaudRate = 115200;
            //Sio.ReadBufferSize = 35;
            Sio.PortName = "COM3";
            Sio.Parity = Parity.None; //space
            Sio.StopBits = StopBits.One;
            Sio.Handshake = Handshake.None;
            Sio.ReadTimeout = 5;
            Sio.WriteTimeout = 5;
            Sio.ParityReplace = (byte)'\0';

            if (!Sio.IsOpen)
            {
                try
                {
                    Sio.Open();
                    tbRX.Text = "Port Offen\n";
                }
                catch { }
            }
            else
            {
                tbRX.Text = "Port beschäftigt\n";
            }
        }

        public static void sp_SerialErrorReceivedEventHandler(Object sender, SerialErrorReceivedEventArgs e)
        {
            if (e.EventType == SerialError.RXParity)
            {
                //tbRX.Text += "Parity error\n";
                //throw new Exception("Parity Error");
            }

            if (e.EventType == SerialError.Frame)
            {
                //tbRX.Text += "Parity error\n";
                throw new Exception("Frame Error");
            }
        }

        private void send_new_Protokoll(Data_new_Protokoll data)
        {
            Sio.DiscardOutBuffer();
            set_parity(Parity.Mark);

            Sio.Write(data.Allbytes, 0, 1);

            set_parity(Parity.Space);
            Sio.Write(data.Allbytes, 1, data.Length + 2);

            tbRX.Text += "Tx_Data gesendet\n";
            //set_parity(Parity.Mark);
        }

        private void set_parity(Parity parity)
        {
            Sio.Close();

            Sio.Parity = parity;

            Sio.Open();
            Sio.DiscardInBuffer();
            Sio.DiscardOutBuffer();
        }

        private byte[] Challengeresponse(byte[] challenge)
        {
            Byte[] response = new byte[8];
            Byte[] key = { 0x87};

            //bytweise berechnung einfügen


            return response;
        }

        private byte[] reziseByte(byte[] bytearr)
        {
            int endindex = bytearr.Length - 1;
            while (endindex > 0 && bytearr[endindex] == 0)
                endindex--;

            Array.Resize(ref bytearr, endindex + 1);
            return bytearr;
        }

        private void Sio_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Sio.Read(buff, 0, 35);
            buff = reziseByte(buff);

            RX_Data = new Data_new_Protokoll(buff);
            buff = new byte[35];

            if (RX_Data.crc_check == true && RX_Data.Command_ID == 0x05 && RX_Data.Databytes[0] == 0x55)
            {
                remaining_init_commands = 0;
                datarecieved_flag = true;
            }

            if (RX_Data.crc_check == true && RX_Data.Command_ID == 0x07 && RX_Data.Databytes[0] == 0x11 && RX_Data.Length == 9)
            {
                byte[] Challenge64Data = new byte[8];
                byte[] Challenge64Answer = new byte[8];

                for (int i = 0; i < 8; i++)
                {
                    Challenge64Data[i] = RX_Data.Databytes[i + 1];
                }
                Challenge64Answer = SolveChallenge(Challenge64Data);
                Challenge_flag = true;
            }

            this.Invoke(new EventHandler(Updatetxt));
        }

        //überarbeiten
        private byte[] SolveChallenge(byte[] challenge)
        {
            byte[] answer = new byte[8];
            return answer;
        }
        private void Updatetxt(object sender, EventArgs e)
        {
            if(RX_Data.Allbytes != null)
            {
                foreach (var el in RX_Data.Allbytes)
                {
                    tbRX.Text += el.ToString() + " ";
                }
                tbRX.Text += "\n";
            }
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            remaining_init_commands = 3;           
            tSleep.Start();
        }

        private void tSleep_Tick(object sender, EventArgs e)
        {
            if (datarecieved_flag == true)
            {
                try
                {
                    send_new_Protokoll(Enter_Production_Mode_Request);
                }
                catch { }
                datarecieved_flag = false;
            }

            else if (Challenge_flag == true)
            {
                try
                {
                    send_new_Protokoll(Callenge_Request);
                }
                catch { }
                tSleep.Stop();
                Challenge_flag = false;
            }
            else if(remaining_init_commands > 1)
            {
                Byte[] data = new byte[] { 0x51 };
                try
                {
                    Sio.DiscardOutBuffer();
                    set_parity(Parity.Mark);
                    Sio.Write(data, 0, 1);
                    tbRX.Text += "Tx_Data gesendet\n";
                }
                catch { }

                remaining_init_commands -= 1;
            }
            else if(remaining_init_commands == 1)
            {
                send_new_Protokoll(Enter_Production_Mode_Request);
                test = RX_Data;
                tbRX.Text += "Verbindung zum DUT konnte nicht hergestellt werden. Bitter erneut versuchen\n";
                remaining_init_commands -= 1;
            }
            else
            {
                test = RX_Data;
                tbRX.Text += "Verbindung zum DUT konnte nicht hergestellt werden. Bitter erneut versuchen\n";
                tSleep.Stop();
            }
        }
    }

    class Data_new_Protokoll
    {
        public Byte[] Allbytes;
        public Byte[] Databytes;
        public int Length = 0x00;
        public Byte Command_ID = 0x00;
        public Byte crc = 0x00;

        public bool crc_check;

        //public void rearangebytearr()
        //{
        //    BitArray allbits = new BitArray(Allbytes);
        //    bool x = allbits[0];
        //    for (int i = 0; i < Allbytes.Length + 2; i++)
        //    {
                
        //    }
        //}

        private void Decode()
        {
            //Checkup
            Command_ID = Allbytes[0];
            Length = (int)Allbytes[1];
            Databytes = new byte[Length];
            try
            {
                for (int i = 0; i < Length; i++)
                {
                    Databytes[i] = Allbytes[i + 2];
                }
                crc = Allbytes[Length + 2];

                Byte[] CRC_Buff = new byte[Length + 2];
                for (int i = 0; i < Length + 2; i++)
                {
                    CRC_Buff[i] = Allbytes[i];
                }

                if (crc == compute_crc(CRC_Buff))
                {
                    crc_check = true;
                }
                else
                {
                    crc_check = false;
                    //throw new Exception("CRC Stimmt nicht");
                }
            }
            catch
            {
                clear();
                //throw new Exception("Bytes konnten nicht konvertiert werden");                
            }           

        }

        public static byte compute_crc(byte[] data)
        {
            byte crc = 0xFF;

            int i, bit;

            for (i = 0; i < data.Length; i++)
            {
                crc ^= data[i];
                for (bit = 0; bit < 8; bit++)
                {
                    if ((crc & 0x80) != 0)
                    {
                        crc <<= 1;
                        crc ^= 0x1D;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            i = (int)crc;
            i = ~i;

            return (byte)i;
        }

        public void clear()
        {
            Allbytes = null;
            Databytes = new byte[32];
            Length = 0x00;
            Command_ID = 0x00;
            crc = 0x00;
        }

        //Konstruktor zum empfangen: Crc wird überprüft
        public Data_new_Protokoll(byte[] byte_buff)
        {
            this.Allbytes = byte_buff;
            //rearangebytearr();
            if (byte_buff != null)
            {
                Decode();
            }
        }

        //Konstruktor zum Senden: Crc wird hier berechnet
        public Data_new_Protokoll(byte cmd_ID, byte[] data_bytes)
        {
            this.Command_ID = cmd_ID;

            //databytes vorhanden
            if (data_bytes != null)
            {
                this.Length = data_bytes.Length;
                this.Allbytes = new byte[Length + 2];
                this.Allbytes[1] = (byte)data_bytes.Length;
                for (int i = 0; i < data_bytes.Length; i++)
                {
                    this.Allbytes[i + 2] = data_bytes[i];
                }
            }

            //keine Databytes vorhanden
            else
            {
                this.Length = 0;
                this.Allbytes = new byte[Length + 2];
                this.Allbytes[1] = 0;
            }
            this.Allbytes[0] = cmd_ID;

            this.Databytes = data_bytes;
            this.crc = Data_new_Protokoll.compute_crc(Allbytes);
            Array.Resize<byte>(ref Allbytes, Length + 3);
            this.Allbytes[Length + 2] = this.crc;
            this.crc_check = true;
        }
    } //Verschönern (Rechte und Sachen in Konstruktor)
}
