﻿
namespace UART_Testgelände
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bSend = new System.Windows.Forms.Button();
            this.tbRX = new System.Windows.Forms.RichTextBox();
            this.bClear = new System.Windows.Forms.Button();
            this.Sio = new System.IO.Ports.SerialPort(this.components);
            this.tSleep = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(660, 56);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(75, 23);
            this.bSend.TabIndex = 0;
            this.bSend.Text = "Send";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // tbRX
            // 
            this.tbRX.Location = new System.Drawing.Point(48, 56);
            this.tbRX.Name = "tbRX";
            this.tbRX.Size = new System.Drawing.Size(590, 360);
            this.tbRX.TabIndex = 1;
            this.tbRX.Text = "";
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(660, 392);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 0;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            // 
            // Sio
            // 
            this.Sio.BaudRate = 4800;
            this.Sio.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.Sio_DataReceived);
            // 
            // tSleep
            // 
            this.tSleep.Interval = 1000;
            this.tSleep.Tick += new System.EventHandler(this.tSleep_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.tbRX);
            this.Controls.Add(this.bSend);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.RichTextBox tbRX;
        private System.Windows.Forms.Button bClear;
        private System.IO.Ports.SerialPort Sio;
        private System.Windows.Forms.Timer tSleep;
    }
}

